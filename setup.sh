#!/bin/bash
#################################
# Author: Eunchong Kim
# Email: eunchong.kim at cern.ch
# Date: Dec. 2019
# Project: ATLAS Setup
# Description: Setup athena
#################################

function usage() {
    echo -e "Usage) source setup.sh a g p"
    echo -e "\th help"
    echo -e "\ta athena : a / athena <version>"
    echo -e "\taa AthAnalysis : aa / AthAnalysis <version>"
    echo -e "\tar restore : 'asetup --restore'"
    echo -e "\trc RootCore : rc / RootCore <version>"
    echo -e "\tg git : lsetup git"
    echo -e "\tp panda : letup panda\n"
}

if [ -z $1 ]; then
    echo -e "Wrong usage!"
    usage
    return
fi

function setupAtlas() {
    if [ -z "$has_setup_atlas" ]; then
        echo -e "\n[SETUP] setupATLAS"
        setupATLAS
        has_setup_atlas="yes"
    fi
}

function vomsInit() {
    if [ -z "$has_voms_init" ]; then
        echo -e "\n[SETUP] voms-proxy-init -voms atlas"
        echo -e "\nvoms-proxy-init -voms atlas"
        voms-proxy-init -voms atlas
        has_voms_init="yes"
    fi
}

while [ ! -z $1 ]; do
    setupAtlas
    PARAM=`echo $1 | awk -F= '{print $1}'`
    case $PARAM in
        h | help)
            usage
            ;;
        a | athena)
            if [[ $2 == *"."* ]]; then athenaVer=$2
            else athenaVer=`ls -v /cvmfs/atlas.cern.ch/repo/sw/software/21.0/Athena/ | tail -n1`
            fi
            echo -e "\n[SETUP] Athena ${athenaVer}"
            asetup ${athenaVer} Athena, here
            ;;
        aa | athanalysis)
            if [[ $2 == *"."* ]]; then athAnaVer=$2
            else athAnaVer=`ls -v /cvmfs/atlas.cern.ch/repo/sw/software/21.2/AthAnalysis/ | tail -n1`
            fi
            echo -e "\n[SETUP] AthAnalysis ${athAnaVer}"
            asetup ${athAnaVer} AthAnalysis, here
            ;;
        ar | restore)
            echo -e "\n[SETUP] restore"
            asetup --restore
            ;;
        rc | RootCore)
            if [[ $2 == *"."* ]]; then rcVer=$2
            else rcVer=`ls -v /cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisBase/ | tail -n1`
            fi
            echo -e "\n[SETUP] RootCore ${rcVer}"
            rcSetup Base, ${rcVer} #TODO use asetup
            ;;
        g | git)
            echo -e "\n[SETUP] git"
            lsetup git
            ;;
        r | rucio)
            echo -e "\n[SETUP] rucio"
            lsetup rucio
            vomsInit
            ;;
        p | panda)
            echo -e "\n[SETUP] Panda"
            lsetup panda
            vomsInit
            ;;
        *)
            echo -e "Wrong usage!"
            usage
            return
            ;;
    esac
    shift
done
