#!/bin/bash

DATE=`date +%y%m%d-%H%M`

DataSetName=dEdxDrop.DS.2017
#inputDataSetName=user.eunchong:user.eunchong.${DataSetName}
inputDataSetName=data16_13TeV:data16_13TeV.00303208.express_express.merge.RAW
outputDataSetName=user.eunchong.00303208.$DATE/

cmake .. || (echo "cmake failed." && exit 1)

source x86_64-slc6-gcc62-opt/setup.sh

gmake -j4 || (echo "gmake failed." && exit 1)

pathena \
  --trf "Reco_tf.py \
    --AMIConfig 'f846' \
    --autoConfiguration='everything' \
    --beamType 'collisions' \
    --ignorePatterns='ToolSvc.InDetSCTRodDecoder.+ERROR.+Unknown.+offlineId.+for.+OnlineId' \
    --ignoreErrors 'True' \
    --maxEvents '-1' \
    --preExec 'all:DQMonFlags.doCTPMon=False;' \
              'DQMonFlags.doLVL1CaloMon=False;' \
              'DQMonFlags.doHLTMon=False;' \
              'DQMonFlags.doTRTMon=False;' \
              'DQMonFlags.doMissingEtMon=False;' \
              'DQMonFlags.doMuonTrackMon=False;' \
              'DQMonFlags.doMuonSegmentMon=False;' \
              'DQMonFlags.doMuonTrkPhysMon=False;' \
              'DQMonFlags.doMuonCombinedMon=False;' \
              'DQMonFlags.doLucidMon=False;' \
              'DQMonFlags.doJetTagMon=False;' \
              'DQMonFlags.doEgammaMon=False;' \
              'DQMonFlags.doMuonRawMon=False;' \
              'DQMonFlags.doTRTElectronMon=False;' \
              'DQMonFlags.doLArMon=False;' \
              'DQMonFlags.doTileMon=False;' \
              'DQMonFlags.doCaloMon=False;' \
              'DQMonFlags.doPixelMon=True;' \
              'DQMonFlags.doGlobalMon=False;' \
              'DQMonFlags.doInDetAlignMon=False;' \
              'DQMonFlags.doInDetGlobalMon=False;' \
              'DQMonFlags.doSCTMon=False;' \
              'DQMonFlags.doInDetPerfMon=False;' \
              'rec.doFwdRegion=False;' \
              'rec.doTau=False;' \
              'rec.doMuon=False;' \
              'rec.doEgamma=False;' \
              'rec.doMuonCombined=False;' \
              'rec.doCalo=False;' \
              'rec.doJetMissingETTag=False;' \
              'rec.doTrigger=False' \
    --inputBSFile %IN \
    --outputHISTFile '%OUT.HIST.root' \
    --outputFileValidation 'False'" \
  --inDS $inputDataSetName \
  --inputFileList ../runList.txt \
  --outDS $outputDataSetName \
  --nFilesPerJob=1



#    --AMIConfig 'f781' \ #for data16
#  --athenaTag=Athena,21.0.30 \
#  --cmtConfig=x86_64-slc6-gcc62-opt \
#    --AMITag 'x486' \
#    --geometryVersion all:ATLAS-R2-2015-04-00-00 \
#    --conditionsTag all:CONDBR2-BLKPA-2016-21 \
#  --site "TOKYO-LCG2_LOCALGROUPDISK" \
#    --postInclude 'all:PixelNoiseMap_1.py' \
#    --postExec  'r2e:topSequence.LArNoisyROAlg.Tool.BadChanPerFEB=30' \
#   --extFile=noisemap_IBL_1.db,PixelNoiseMap_1.py \
#  --outDS "user.eunchong.data16_13TeV.00303208.express_express.merge.HIST.$DATE/" \
