#!/bin/bash

set -e # Exit immediately if a command exits with a non-zero status
DATADIR=../../data/data17_13TeV/
DATANAME=data17_13TeV.00340072.express_express.merge.RAW._lb0400._SFO-ALL._0001.1
DATE=`date +%y%m%d-%H%M`
NUMOFEVENTS=5
AMI_TAG=f933

echo " "
echo "Run cmake ..."
echo " "
cmake .. || (echo "cmake failed." && exit 1)

source x86_64-slc6-gcc62-opt/setup.sh


echo " "
echo "Run make -j4 ..."
echo " "
gmake -j4 || (echo "gmake failed." && exit 1)


echo " "
echo "Starting ATHENA run ..."
echo " "

/usr/bin/time -v  Reco_tf.py \
    --conditionsTag all:CONDBR2-BLKPA-2018-09 \
    --ignoreErrors 'False' \
    --autoConfiguration='everything' \
    --AMITag $AMI_TAG \
    --postExec 'r2a:MSMgr.AddItemToAllStreams("xAOD::MuonAuxContainer#MuonsAux.-DFCommonMuonsTight.-DFCommonGoodMuon.-DFCommonMuonsMedium.-DFCommonMuonsLoose")' \
    --preExec 'r2a:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.useDynamicAlignFolders.set_Value_and_Lock(True);TriggerFlags.AODEDMSet="AODFULL";rec.doAFP.set_Value_and_Lock(True);DQMonFlags.doAFPMon=True;' 'all:DQMonFlags.doCTPMon=False;' 'DQMonFlags.doLVL1CaloMon=False;' 'DQMonFlags.doHLTMon=False;' 'DQMonFlags.doTRTMon=False;' 'DQMonFlags.doSCTMon=False;' 'DQMonFlags.doMissingEtMon=False;' 'DQMonFlags.doMuonTrackMon=False;' 'DQMonFlags.doMuonSegmentMon=False;' 'DQMonFlags.doMuonTrkPhysMon=False;' 'DQMonFlags.doMuonCombinedMon=False;' 'DQMonFlags.doLucidMon=False;' 'DQMonFlags.doJetTagMon=False;' 'DQMonFlags.doEgammaMon=False;' 'DQMonFlags.doMuonRawMon=False;' 'DQMonFlags.doTRTElectronMon=False;' 'DQMonFlags.doLArMon=False;' 'DQMonFlags.doTileMon=False;' 'DQMonFlags.doCaloMon=False;' 'DQMonFlags.doPixelMon=True;' 'DQMonFlags.doGlobalMon=False;' 'DQMonFlags.doInDetAlignMon=False;' 'DQMonFlags.doInDetGlobalMon=False;' 'DQMonFlags.doInDetPerfMon=False;' 'rec.doFwdRegion=False;' 'rec.doTau=False;' 'rec.doMuon=False;' 'rec.doEgamma=False;' 'rec.doMuonCombined=False;' 'rec.doCalo=False;' 'rec.doJetMissingETTag=False;' 'rec.doTrigger=False' \
    --geometryVersion all:ATLAS-R2-2016-01-00-01 \
    --steering='doRAWtoALL' \
    --beamType 'collisions' \
    --inputBSFile "$DATADIR/$DATANAME" \
    --outputHISTFile HIST.$DATE.${DATANAME:0:55}.root \
    --outputFileValidation 'False' \
    --maxEvents "$NUMOFEVENTS"
