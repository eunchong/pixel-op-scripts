#!/bin/bash
# Description   : Run noise map tool
# Author        : Eunchong Kim
# Date          : 2019-07-01

date=`date +%y%m%d_%H%M`

data_path=/home/eckim/data/data18_13TeV/data18_13TeV.00364214.express_express.merge.RAW._lb0720._SFO-ALL._0001.1
inputDataSetName="data18_13TeV:data18_13TeV.00364214.express_express.merge.RAW"
outputDataSetName="user.eunchong.NoiseMap_00364214_express_express_${date}"

stdout_file_name=output_${date}.log
stderr_file_name=stderr_${date}.log
nevents=-1
AMI_TAG=f933

function usage() {
    echo -e "Usage) ./run_noise_map_builder.sh ~"
    echo -e "\th help"
    echo -e "\tp pathena: use pathena"
    echo -e "\tf fileInput <file path>:  parse input file from command line"
    echo -e "\t*: default\n"
}

set -e # Exit immediately if a command exits with a non-zero status

echo -e "Set std log file path: $PWD/${stdout_file_name}\n"
echo -e "Set err log file path: $PWD/${stderr_file_name}\n"

if [ ! -e NoiseMapBuilder.py ]; then
    echo -e "No 'NoiseMapBuilder.py' found! Copy file from original NoiseMapBuilder.py ...\n"
    cp ../InnerDetector/InDetCalibAlgs/PixelCalibAlgs/share/NoiseMapBuilder.py .
fi

echo -e "Set # of events to run in 'NoiseMapBuilder.py': $nevents\n"
line_number=`grep -e 'nevents          =' -n NoiseMapBuilder.py | sed -e 's/:.*//g'` # Get line cotains 'nevents          ='
sed -i "${line_number}d" NoiseMapBuilder.py
sed_to="nevents          =$nevents"
sed -i "${line_number}i $sed_to" NoiseMapBuilder.py


echo -e "\nRun cmake ...\n"
cmake .. || (echo "cmake failed." && exit 1)


#source x86_64-slc6-gcc62-opt/setup.sh
source x86_64-centos7-gcc62-opt/setup.sh || (echo "source failed." && exit 1) # for ccl7


echo -e "\nRun make -j4 ...\n"
gmake -j4 || (echo "gmake failed." && exit 1)


echo -e "Set output root file name in 'NoiseMapBuilder.py': NoiseMap_${date}.root\n"
line_number=`grep -e 'histfile DATAFILE=' -n NoiseMapBuilder.py | sed -e 's/:.*//g'` # Get line cotains 'nevents          ='
sed -i "${line_number}d" NoiseMapBuilder.py
sed_to="THistSvc.Output += [\"histfile DATAFILE='NoiseMap_${date}.root' OPT='RECREATE'\"]"
sed -i "${line_number}i $sed_to" NoiseMapBuilder.py


echo -e "\nRun ATHENA ...\n"
PARAM=`echo $1 | awk -F= '{print $1}'`
case $PARAM in
    h | help)
        usage
        ;;
    p | pathena)
        echo -e "\n[RUN] Use pathena"
        pathena \
            NoiseMapBuilder.py\
            --inDS $inputDataSetName \
            --outDS $outputDataSetName
        ;;
    f | filesInput)
        echo -e "\n[RUN] Use athena with args"
        echo $data_path > inputfilelist
        /usr/bin/time -v athena.py NoiseMapBuilder.py --filesInput $data_path > >(tee $stdout_file_name) 2> >(tee $stderr_file_name >&2)
        ;;
    *)
        echo -e "\n[RUN] Use athena with inputfilelist"
        /usr/bin/time -v athena.py NoiseMapBuilder.py > >(tee $stdout_file_name) 2> >(tee $stderr_file_name >&2)
        #/usr/bin/time -v athena.py NoiseMapBuilder.py 2>&1 | tee -a $stdout_file_name
        #/usr/bin/time -v athena.py NoiseMapBuilder.py > hoge.log 2> error.log | watch -n 2 "tail -n 20 -v hoge.log && echo -e '\n\n\n\n\n\n' && tail -n 10 -v error.log"
        ;;
esac
shift
