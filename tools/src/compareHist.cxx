#include <fstream>
#include <cstdio>
#include <string>
#include <sstream>
#include <iostream>
#include <cmath>

#include "TF1.h"
#include "TH1F.h"
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TFitResult.h"
#include "TLegend.h"
#include "TLine.h"
#include "TPad.h"
#include "TApplication.h"
#include "TROOT.h"

#include "/opt/root/atlasrootstyle/AtlasStyle.h"
#include "/opt/root/atlasrootstyle/AtlasUtils.h"
#include "/opt/root/atlasrootstyle/AtlasLabels.h"

#define NUMBEROFLAYER   4       // ibl, B-layer, layer-1, layer-2
#define NUMBEROFTRY     3       // n of standard error

// Global
Int_t             userRangeMin    [NUMBEROFLAYER] = {0, 7, 17, 17},   
                  userRangeMax    [NUMBEROFLAYER] = {15, 22, 32, 32};  

Int_t             setYaxisRangeMin = 5,  
                  setYaxisRangeMax = 28; 
     
std::string       layerName     [NUMBEROFLAYER] = {"IBL2D", "B0", "B1", "B2"};
std::string       layerName2    [NUMBEROFLAYER] = {"Planar IBL", "L0", "L1", "L2"};
std::string       subsubName    [2] = {"canvas","histograms"};
std::string       methodName    [3] = {"mean_method_all", "mean_method_part", "gaussian_fitting"};

int               xmax          [NUMBEROFLAYER];

TFile             *rootFile;
int               nbin          [NUMBEROFLAYER];
double            fullXaxisRangeMin = -0.5;
double            fullXaxisRangeMax = 299.5;
double            setXaxisRangeMin = -0.5;
double            setXaxisRangeMax = 60.5;

std::stringstream temp_strstream;
std::string       histName = "Cluster_ToTxCosAlpha_";
std::string       atext = "ToT"; 

// Use for read run list
std::string         run_list_name = "";
int                 num_of_run = 0;
std::vector<double> run_num;
std::vector<std::string> run_name;

// Use for read luminosity
std::vector<double> lumi;
std::vector<double> lb; // LB

// Read histograms
std::vector<std::vector<TH1F*>> h1D;
std::vector<std::vector<TH1F*>> h1D_norm;

// Put peaks
std::vector<std::vector<double>> peak, peak_sys_err, peak_sta_err;
std::vector<std::vector<std::vector<double>>> peak_save;

// Scan ToT
std::vector<double> scan_tot, scan_lumi;

void readRunList(std::string run_list_path){
  std::ifstream run_list(run_list_path.c_str());
  
  if (!run_list.is_open()) {
    std::cerr << "Error open run list!" << std::endl;
    exit(1);
  }
  getline(run_list, run_list_name);
//  run_list >> run_list_name; // First line is run list name
  std::cout << "Run list name is: "<< run_list_name << std::endl;

  run_list >> num_of_run; // Second line is # of runs
  std::cout << "# of run is: "<< num_of_run << std::endl;

  for (int i=0; i<num_of_run; i++) {
    //double temp;
    std::string temp;
    run_list >> temp;
    //getline(run_list, temp);
    run_name.push_back(temp);
    run_num.push_back(std::stod(temp.substr(15,6)));
    std::cout << "run # is: "<< temp.substr(15,6) << std::endl;
  }
}
 
void getLuminosity(){
  std::cout << "Read Luminosity. " << std::endl;

  for (int r=0; r<num_of_run; r++) {
    char buffer_char[512];
    temp_strstream.str(""); temp_strstream << "grep -rnw " << (int)run_num[r] << " runsInfo.txt | cut -d ' ' -f4";
    FILE *result_of_grep = popen(temp_strstream.str().c_str(), "r");
    fgets(buffer_char, sizeof(buffer_char), result_of_grep);
    lumi.push_back(atof(buffer_char));
    lumi[r] *= 1E-9;
    lumi[r] += 28.26; // Add run-1's deli lumi
  }
}

void legendSetup(TLegend *lege) {
  lege ->SetFillStyle(0);
  lege ->SetFillColor(0);
  lege ->SetBorderSize(0);
}

void resizeVectors() {
  std::cout << "Resize Vectors." << std::endl;

  // Set # run size of h1D
  h1D.resize(num_of_run);
  h1D_norm.resize(num_of_run);
  peak.resize(num_of_run);
  peak_sys_err.resize(num_of_run);
  peak_sta_err.resize(num_of_run);
  peak_save.resize(num_of_run);
  lb.resize(num_of_run);

  for (int r=0; r<num_of_run; r++) {
    // Set layers size of h1D
    h1D[r].resize(NUMBEROFLAYER);
    h1D_norm[r].resize(NUMBEROFLAYER);
    peak[r].resize(NUMBEROFLAYER);
    peak_sys_err[r].resize(NUMBEROFLAYER);
    peak_sta_err[r].resize(NUMBEROFLAYER);
    peak_save[r].resize(NUMBEROFLAYER);

    for (int l=0; l<NUMBEROFLAYER; l++)
      peak_save[r][l].resize(NUMBEROFTRY);
  }
}

void getHist() {
  std::cout << "Read histograms." << std::endl;

  std::string path = "Pixel/ClustersOnTrack/";

  for (int r=0; r<num_of_run; r++) {
    // Open root file 
    //temp_strstream.str(""); temp_strstream  << "data/HIST.00" << run_num[r] << ".root";
    temp_strstream.str(""); temp_strstream  << "data/HIST." << run_name[r] << ".root";
    TFile *file = new TFile(temp_strstream.str().c_str(), "read");
    if (!file->IsOpen()) {
      std::cout << "Cannot open root file: " << temp_strstream.str() << std::endl;
      exit(1);
    }

    // Get histgram from root file 
    for (int l=0; l<NUMBEROFLAYER; l++) {
      temp_strstream.str(""); temp_strstream  << "run_" << run_num[r] << "/" << path << histName << layerName[l];
std::cout << temp_strstream.str() << std::endl;
      h1D[r][l] = (TH1F*)file->Get(temp_strstream.str().c_str()); 
      h1D_norm[r][l] = h1D[r][l];
    }

    // Get LBs
    temp_strstream.str(""); temp_strstream  << "run_" << run_num[r] << "/" << path << "Clusters_per_lumi_" << layerName[1];
    TH1F *temp_h1D = (TH1F*)file->Get(temp_strstream.str().c_str()); 
    lb[r] = temp_h1D->GetMean();
  }
}

void saveOriHist(){
  std::cout << "Save original hitograms." << std::endl;

  for(int l=0; l<NUMBEROFLAYER; l++){
    // Save to root 
    int d = 1; // histograms
    temp_strstream.str("");  temp_strstream << layerName2[l] << "/" << histName << "/" << subsubName[d];
    rootFile->cd(temp_strstream.str().c_str());

    for(int r=0; r<num_of_run; r++){
      temp_strstream.str(""); temp_strstream << run_num[r];
      h1D[r][l]->GetXaxis()->SetRangeUser(setXaxisRangeMin, setXaxisRangeMax);
      h1D[r][l]->Write(temp_strstream.str().c_str());
    }
    rootFile->cd();
  }
}

void normalize(){
  for(int r=0; r<num_of_run; r++){
    for(int l=0; l<NUMBEROFLAYER; l++){
      double maxBin = h1D_norm[r][l]->GetMaximumBin();
      double norm   = h1D_norm[r][l]->GetBinContent(maxBin);
      h1D_norm[r][l]->Scale(1/norm);
    }
  }
}

void getMaxBinNBin(int bin){
  std::cout << "Get Max Bin # " << bin << std::endl;

  for(int r=0; r<num_of_run; r++) {
    for(int l=0; l<NUMBEROFLAYER; l++) {
      h1D[r][l]->GetXaxis()->SetRangeUser(fullXaxisRangeMin, fullXaxisRangeMax);
      int max_bin = h1D[r][l]->GetMaximumBin();
      if (r == 0) xmax[l] = max_bin; 
      if (l == 0) nbin[l] = 4+bin;// for IBL
      else        nbin[l] = 6+bin;// for layers
    }
  }
}

////calculate peak
//void calculatePeak(int t){
//  for (int r=0; r<num_of_run; r++) {
//    for (int l=0; l<NUMBEROFLAYER; l++) {
//      double a = 0; 
//      double b = 0;
//      int nshift = 0;
//      if (h == 0 && l == 0) nshift = 0; //IBL, shift 1 bin
//      else nshift = -1; // B0,1,2, shift 3 bin
//      for (int i=0; i<nbin[l]; i++) {
//        double x = h1D[r][l]->GetXaxis()->GetBinCenter(xmax[l] - nbin[l]/2 + i + nshift);
//        double y = h1D[r][l]->GetBinContent(xmax[l] - nbin[l]/2 + i + nshift); 
//        a += x * y;
//        b += y;
//      }
//      double mean       = a/b;
//      peak[r][l]     = mean;
//      peakSave[r][l][t]  = peak[r][l];
//    } 
//  }
//}

//took mean
//void tookMean(int t){
//  for (int r=0; r<num_of_run; r++) {
//    for (int l=0; l<NUMBEROFLAYER; l++) {
//      h1D[r][l]         ->GetXaxis()->SetRangeUser(setXaxisRangeMin, setXaxisRangeMax);
//      peak[r][l]         = h1D[r][l]->GetMean(1);
//      peakSave[r][l][t]  = peak[r][l];
//    } 
//  }
//}

void fitPeak(bool write2file, int t) {
  int               fitMin;
  int               fitMax;

  std::cout << "Fit peak" << std::endl;

  for (int r=0; r<num_of_run; r++) {
    for (int l=0; l<NUMBEROFLAYER; l++) {
      // Set fit range
      h1D[r][l]->GetXaxis()->SetRangeUser(userRangeMin[l], userRangeMax[l]);
      h1D[r][l]->GetXaxis()->SetRangeUser(fullXaxisRangeMin,fullXaxisRangeMax);
      fitMin = h1D[r][l]->GetXaxis()->GetBinCenter(xmax[l]-nbin[l]/2);
      fitMax = h1D[r][l]->GetXaxis()->GetBinCenter(xmax[l]+nbin[l]/2);

      // Fit
      TFitResultPtr       fitResult; 
      fitResult           = h1D[r][l]->Fit("gaus", "SQ", "", fitMin, fitMax );
      peak[r][l]          = fitResult->Parameter(1); 
      peak_save[r][l][t]  = peak[r][l];
      peak_sta_err[r][l]  = fitResult->ParError(1);

      // Save to root
      if (write2file) {
        int d = 1; // histograms
        temp_strstream.str("");  temp_strstream << layerName2[l] << "/" << histName << "/" << subsubName[d];
        rootFile->cd(temp_strstream.str().c_str());

        h1D[r][l]->SetMinimum(0);
        temp_strstream.str("");  temp_strstream << run_num[r] << "_" << "gaus" << "_" << nbin[l] << "bins";
        h1D[r][l]->Write(temp_strstream.str().c_str());

        rootFile->cd();
      }
    } 
  }
}

// standard error
void calculateError() {
  std::cout << "Calculate Error" << std::endl;

  for (int r=0; r<num_of_run; r++) {
    for (int l=0; l<NUMBEROFLAYER; l++) {
      double temp = 0;
      peak_sys_err[r][l] = 0;
      for (int t=0; t<NUMBEROFTRY; t++) {
        temp += peak_save[r][l][t];
        //if (l == 1) std::cout << peak_save[r][l][t] << std::endl;
      }
      temp /= NUMBEROFTRY;
      for (int t=0; t<NUMBEROFTRY; t++) peak_sys_err[r][l] += pow(std::abs(temp-peak_save[r][l][t]), 2);
      peak_sys_err[r][l] = sqrt(peak_sys_err[r][l]/(NUMBEROFTRY-1));
    }
  }
}

void plotHist(){
// Plot 1 histograms on 1 can
// and plot 8 histograms on 1 can
  std::cout << "Plot histograms" << std::endl;

  TCanvas           *tot_canv[5][NUMBEROFLAYER];
  TCanvas           *canv;
  TLegend           *legend;

  temp_strstream.str(""); temp_strstream << run_list_name << histName << "_" << layerName[0] << "_added";
  canv = new TCanvas(temp_strstream.str().c_str(), temp_strstream.str().c_str(), 600, 600);
  for (int r=0; r<num_of_run; r++) {
    for (int l=0; l<NUMBEROFLAYER; l++) {
      // Construct TCanvas
      TCanvas *can;
      temp_strstream.str(""); temp_strstream << "run_" << run_num[r] << "_" << histName << "_" << layerName[l];
      can = new TCanvas(temp_strstream.str().c_str(), temp_strstream.str().c_str(), 600, 600);
      if (r%8 == 0) {
        temp_strstream << "_sheet";
        tot_canv[r/8][l] = new TCanvas(temp_strstream.str().c_str(), temp_strstream.str().c_str(), 1920, 1080);
        tot_canv[r/8][l] ->Divide(2, 4);
      }

      // Draw 
      can->cd();
      if (l == 0) h1D[r][l] ->GetXaxis()->SetRangeUser(setXaxisRangeMin, 30.5);
      else        h1D[r][l] ->GetXaxis()->SetRangeUser(setXaxisRangeMin, setXaxisRangeMax);
      h1D[r][l] ->SetTitle("");
      h1D[r][l] ->SetMinimum(0);
      h1D[r][l] ->Draw("H");  

      // plot peak line
//      double maxBin    = h1D[r][l]->GetMaximumBin();
//      double ymax_temp = h1D[r][l]->GetBinContent(maxBin);
//      TLine * line  = new TLine(peak[r][l], 0, peak[r][l], ymax_temp*1.05);
//      line ->SetLineStyle(2);
//      line ->SetLineColor(2);
//      line ->SetLineWidth(2);
//      line ->Draw();
//      temp_strstream.str("");  temp_strstream << std::fixed << std::setprecision(2) << peak[r][l] ;
//      if (l == 0)       myText(0.2, 0.2, 2, temp_strstream.str().c_str());
//      else if (l == 1)  myText(0.4, 0.2, 2, temp_strstream.str().c_str());
//      else              myText(0.6, 0.2, 2, temp_strstream.str().c_str());
      // Draw Run #
      myText(0.7, 0.8, 0, run_name[r].c_str());

      // Plot several tot distributions
      if (r%8 < 4) tot_canv[r/8][l] ->cd(((r%8)*2)%8+1);
      else         tot_canv[r/8][l] ->cd(((r%8)*2)%8+2);
      gPad->SetTopMargin(0.000001);
      gPad->SetBottomMargin(0.1);
      h1D[r][l] ->GetXaxis()->SetTitleOffset(2);
      h1D[r][l] ->GetYaxis()->SetTitleOffset(4);
      h1D[r][l] ->Draw("H");  
//      line ->Draw();
//      temp_strstream.str("");  temp_strstream << std::fixed << std::setprecision(2) << peak[r][l] ;
//      myText(0.4, 0.2, 2, temp_strstream.str().c_str());
      std::size_t pos;
      if (r < 3)  pos = run_name[r].find("lb");
      else        pos = run_name[r].find("f9");
      myText(0.7, 0.8, 1, run_name[r].substr(pos).c_str());

      // Over write
      if (l == 0) {
        canv->cd();
//        h1D_norm[r][l]->GetFunction("gaus")->SetBit(TF1::kNotDraw);
        h1D_norm[r][l]->SetLineColor(r+1);
        if (r == 0) h1D_norm[r][l]->Draw("HIST");  
        else        h1D_norm[r][l]->Draw("HISTSAME");  
  
        // Legend
        if (r == 0) {
          legend = new TLegend(0.6, 0.6, 0.9, 0.9);      
          legendSetup(legend);
        }

        legend ->AddEntry(h1D[r][l], run_name[r].substr(pos).c_str(), "l");

        if (r == num_of_run-1) {
          legend->Draw();
        }
      }
 
      // Print pdf file, write to root file
      int d = 0;
      temp_strstream.str("");  temp_strstream << layerName2[l] << "/" << histName << "/" << subsubName[d];
      rootFile->cd(temp_strstream.str().c_str());
      temp_strstream.str("");  temp_strstream << run_num[r] << "_" << nbin[l] << "bins" ;
      can ->Write(temp_strstream.str().c_str());
      // several tot distributions
      if (r%8 == 7 || r == num_of_run-1) {
        temp_strstream.str(""); temp_strstream << "result/" << run_list_name << "_" << histName << layerName[l] << "_" << r/8 << ".pdf";
        tot_canv[r/8][l] ->Write(temp_strstream.str().c_str());
        tot_canv[r/8][l] ->Print(temp_strstream.str().c_str());
      }
      temp_strstream.str(""); temp_strstream << "result/" << run_list_name << "_" << histName << layerName[l] << "_added" << ".pdf";
      canv->Write(temp_strstream.str().c_str());
      canv->Print(temp_strstream.str().c_str());
      rootFile->cd();
      
      delete  can;
    }
  }
}

void plotGraphAllLayer (std::string year, std::string method_name) {
  TGraphErrors          *graph          [NUMBEROFLAYER];
  TCanvas               *canvas         ;
  TPad                  *pad            [3];
  double                padRatioXMin    [3] = {0.0, 0.0, 0.0};
  //double                padRatioYMin    [3] = {0.0, 0.30, 0.60};
  double                padRatioYMin    [3] = {0.0, 0.40, 0.70};
  double                padRatioXMax    [3] = {1.0, 1.0, 1.0};
  //double                padRatioYMax    [3] = {0.30, 0.60, 1.0};
  double                padRatioYMax    [3] = {0.40, 0.70, 1.0};
  std::vector<double>   temp_y, temp_y_err, temp_lumi;
  //double                resultRangeMin  [NUMBEROFLAYER] = {4.48, 12.3, 22.8, 22.8},   
  //                      resultRangeMax  [NUMBEROFLAYER] = {6.6, 15.3, 26.5, 26.5};  
  double                resultRangeMin  [NUMBEROFLAYER] = {3.3, 11.8, 22.8, 22.8},   
                        resultRangeMax  [NUMBEROFLAYER] = {7.0, 15.5, 26.5, 26.5};  
  double                small         = 1e-2;

  std::cout << "Plot all layer graph" << std::endl;

  // Construct canvas 
  temp_strstream.str(""); temp_strstream << histName << "_" << method_name;
  canvas = new TCanvas(temp_strstream.str().c_str(), temp_strstream.str().c_str(), 0, 0, 600, 600);

  // Set 3 pad for IBL, B0, B1+B2
  for (int i=0; i<3; i++) {
    temp_strstream.str(""); temp_strstream << histName << "_" << method_name;
    pad[i] = new TPad(temp_strstream.str().c_str(), temp_strstream.str().c_str(), padRatioXMin[i], padRatioYMin[i], padRatioXMax[i], padRatioYMax[i]);
    pad[i] ->Draw();
  }

  // Make graph 
  for (int l=0; l<NUMBEROFLAYER; l++) {
    temp_y.clear(); temp_y_err.clear(); temp_lumi.clear();
    for (int r=0; r<num_of_run; r++) {
      temp_y.push_back(peak[r][l]);
      temp_y_err.push_back(peak_sys_err[r][l]);
      temp_lumi.push_back(lumi[r]);
    }
    std::cout << "  hoge, l: " << l << ", drop[%]: "<< (peak[6][l]-peak[0][l])/(peak[0][l])
                                        << " +- " << 100*(sqrt(pow(peak_sys_err[6][l],2) + pow(peak_sys_err[0][l],2)))/(peak[0][l]) << std::endl;
    graph[l] = new TGraphErrors(num_of_run, &temp_lumi[0], &temp_y[0], 0, &temp_y_err[0]);
  
    // Set Graph common settings
    graph[l] ->SetTitle("");
    //graph[l] ->GetXaxis() ->SetRangeUser(-.5, 100.5);
    graph[l] ->GetYaxis() ->SetRangeUser(resultRangeMin[l], resultRangeMax[l]);

    // Set Graph setting each layer
    if (l == 3) {         // B2
      graph[l] ->SetLineColor(2);
      graph[l] ->SetMarkerColor(2);
      graph[l] ->SetMarkerStyle(21);
    }
    else if (l == 2) {    // B1
      pad[l] ->cd();
      gPad ->SetBottomMargin(small);
      graph[l] ->GetYaxis() ->SetTitleOffset(1.8);
      graph[l] ->GetYaxis() ->SetTitle(atext.c_str());
    }
    else if (l == 1) {    // B0
      pad[l] -> cd();
      gPad ->SetTopMargin(small);
      gPad ->SetBottomMargin(small);
    } 
    else {                // IBL
      pad[l] -> cd();
      gPad ->SetTopMargin(small);
      gPad ->SetBottomMargin(0.3);
      graph[l] ->GetXaxis() ->SetTitleOffset(2.5);
      graph[l] ->GetXaxis() ->SetTitle(" Delivered luminosity [fb^{-1}]");
    }

    // Plot
    if (l == 3) {         // B2
      graph[l] ->Draw("P");
      myMarkerText( 0.80, 0.80, 2, 21, layerName2[l].c_str(), 1.3);
      //ATLASLabel(0.55,0.80,"Pixel inner");
    }
    else {                // IBL, B0, B1
      graph[l] ->Draw("AP");
      myMarkerText( 0.80, 0.90, 1, 20, layerName2[l].c_str(), 1.3);
    }

    //// border between data 16 and 17
    //if (n == 0) {
    //  TLine *line = new TLine(44.0+28.26, resultRangeMin[l] + yadd, 44.0+28.26, resultRangeMax[l] + yadd);
    //  line ->SetLineWidth(2);
    //  line ->SetLineStyle(3);
    //  line ->SetLineColor(4);
    //  line ->Draw("same");
    //}
  }
  
  // Save canvas as pdf
  temp_strstream.str(""); temp_strstream << "result/" << year + "_" + histName + method_name << ".pdf";
  canvas ->Print(temp_strstream.str().c_str());

  // Save in root file
  temp_strstream.str(""); 
  rootFile ->cd(method_name.c_str());
  temp_strstream << histName << "graph_" << nbin[0] ;
  canvas ->Write( temp_strstream.str().c_str() );
  
  rootFile ->cd();
}

void plotGraphLayer(std::string year, std::string method_name, int layer) {
  TGraphErrors          *graph_sys_err, *graph_all_err;
  TCanvas               *canvas;
  std::vector<double>   temp_x_err, temp_y, temp_y_sys_err, temp_y_all_err, temp_lumi;

  std::cout << "Plot layer " << layerName[layer] << " graph" << std::endl;

  // Construct canvas 
  temp_strstream.str(""); temp_strstream << histName << "_" << method_name;
  canvas = new TCanvas(temp_strstream.str().c_str(), temp_strstream.str().c_str());

  // Make graph 
  int l = layer;
  temp_y.clear(); temp_y_sys_err.clear(); temp_y_all_err.clear(); temp_lumi.clear();
  for (int r=0; r<num_of_run; r++) {
    std::cout << "r: " << r << ", lumi: " << lumi[r] << ", peak: " << peak[r][l] << " +- " << peak_sta_err[r][l] << " +- " << peak_sys_err[r][l] << std::endl;
    temp_y.push_back(peak[r][l]);
    temp_y_sys_err.push_back(peak_sys_err[r][l]);
    temp_y_all_err.push_back(sqrt(pow(peak_sys_err[r][l], 2) + pow(peak_sta_err[r][l], 2)));
    temp_lumi.push_back(lumi[r]);
    temp_x_err.push_back(0.5);
  }
  graph_sys_err = new TGraphErrors(num_of_run, &temp_lumi[0], &temp_y[0], &temp_x_err[0], &temp_y_sys_err[0]);
  graph_all_err = new TGraphErrors(num_of_run, &temp_lumi[0], &temp_y[0], 0, &temp_y_all_err[0]);

  // Set Graph common settings
  graph_sys_err ->SetTitle("");
  if (year.find("2015") != std::string::npos) {
    graph_sys_err ->GetXaxis()->SetLimits(0+28.26, 4.4+28.26);
    graph_sys_err ->GetYaxis()->SetRangeUser(5.8, 9.5);
  }
  else if (year.find("2016") != std::string::npos) {
    graph_sys_err ->GetXaxis()->SetLimits(28.26+4.4-1, 28.26+4.4+44.03);
    graph_sys_err ->GetYaxis()->SetRangeUser(3.8, 7.5);
  }
  else if (year.find("2017") != std::string::npos) {
    graph_sys_err ->GetXaxis()->SetLimits(28.26+4.4+44.03-5, 28.26+4.4+44.03+50);
    graph_sys_err ->GetYaxis()->SetRangeUser(3.3, 7.2);
  }
  graph_sys_err ->GetYaxis()->SetTitle(atext.c_str());
  graph_sys_err ->GetXaxis()->SetTitleOffset(2.5);
  graph_sys_err ->GetXaxis()->SetTitle("Delivered luminosity [fb^{-1}]");
  //canvas->SetBottomMargin(0.3);
  graph_sys_err ->GetXaxis()->SetTitleOffset(1.);

  graph_sys_err->SetFillColor(7);
  graph_sys_err->SetFillStyle(3001);
  canvas ->SetGridx(1);

  // Plot
  graph_sys_err ->Draw("A2");
  graph_all_err ->Draw("P");
  myMarkerText(0.80, 0.90, 1, 20, layerName2[l].c_str(), 1.3);

  // Save canvas as pdf
  temp_strstream.str(""); temp_strstream << "result/" << year + "_" + histName + method_name + layerName[l] << ".pdf";
  canvas ->Print(temp_strstream.str().c_str());

  // Save in root file
  temp_strstream.str(""); temp_strstream <<  histName << "graph_" << nbin[0] ;
  rootFile ->cd(method_name.c_str());
  canvas ->Write( temp_strstream.str().c_str() );
  
  rootFile ->cd();
}

void plotTotVsMuLayer (std::string year, std::string method_name, int layer) {
  TGraphErrors          *graph_sys_err, *graph_all_err;
  TCanvas               *canvas;
  std::vector<double>   temp_x_err, temp_y, temp_y_sys_err, temp_y_all_err, temp_lumi;

  std::cout << "Plot layer " << layerName[layer] << " graph" << std::endl;

  // Construct canvas 
  temp_strstream.str(""); temp_strstream << histName << "_" << method_name;
  canvas = new TCanvas(temp_strstream.str().c_str(), temp_strstream.str().c_str());

  // Make graph 
  int l = layer;
  temp_y.clear(); temp_y_sys_err.clear(); temp_y_all_err.clear(); temp_lumi.clear();
  for (int r=0; r<num_of_run; r++) {
    std::cout << "r: " << r << ", lumi: " << lumi[r] << ", peak: " << peak[r][l] << " +- " << peak_sta_err[r][l] << " +- " << peak_sys_err[r][l] << std::endl;
    temp_y.push_back(peak[r][l]);
    temp_y_sys_err.push_back(peak_sys_err[r][l]);
    temp_y_all_err.push_back(sqrt(pow(peak_sys_err[r][l], 2) + pow(peak_sta_err[r][l], 2)));
    temp_lumi.push_back(lb[r]);
    temp_x_err.push_back(5);
  }
  graph_sys_err = new TGraphErrors(num_of_run, &temp_lumi[0], &temp_y[0], &temp_x_err[0], &temp_y_sys_err[0]);
  graph_all_err = new TGraphErrors(num_of_run, &temp_lumi[0], &temp_y[0], 0, &temp_y_all_err[0]);

  // Set Graph common settings
  graph_sys_err ->SetTitle("");
//  if (year.find("2015") != std::string::npos) {
//    graph_sys_err ->GetXaxis()->SetLimits(0+28.26, 4.4+28.26);
//    graph_sys_err ->GetYaxis()->SetRangeUser(5.8, 9.5);
//  }
//  else if (year.find("2016") != std::string::npos) {
//    graph_sys_err ->GetXaxis()->SetLimits(28.26+4.4-1, 28.26+4.4+44.03);
//    graph_sys_err ->GetYaxis()->SetRangeUser(3.8, 7.5);
//  }
//  else if (year.find("2017") != std::string::npos) {
//    graph_sys_err ->GetXaxis()->SetLimits(28.26+4.4+44.03-5, 28.26+4.4+44.03+50);
    graph_sys_err ->GetYaxis()->SetRangeUser(3.3, 7.2);
//  }
  graph_sys_err ->GetYaxis()->SetTitle(atext.c_str());
  graph_sys_err ->GetXaxis()->SetTitleOffset(2.5);
  graph_sys_err ->GetXaxis()->SetTitle("LBs");
  //canvas->SetBottomMargin(0.3);
  graph_sys_err ->GetXaxis()->SetTitleOffset(1.);

  graph_sys_err->SetFillColor(7);
  graph_sys_err->SetFillStyle(3001);
  canvas ->SetGridx(1);
  canvas ->SetGridy(1);

  // Plot
  graph_sys_err ->Draw("A2");
  graph_all_err ->Draw("P");
  myMarkerText(0.80, 0.90, 1, 20, layerName2[l].c_str(), 1.3);

  // Save canvas as pdf
  temp_strstream.str(""); temp_strstream << "result/" << year + "_" + histName + method_name + layerName[l] << ".pdf";
  canvas ->Print(temp_strstream.str().c_str());

  // Save in root file
  temp_strstream.str(""); temp_strstream <<  histName << "graph_" << nbin[0] ;
  rootFile ->cd(method_name.c_str());
  canvas ->Write( temp_strstream.str().c_str() );
  
  rootFile ->cd();
}

int main(int argc, char *argv[]) {
  gROOT->SetBatch(kTRUE);

  SetAtlasStyle();

  // Read run list
  if (argc < 2) {
    std::cerr << "Need run list !" << std::endl;
    exit(1);
  }
  std::string run_list_path = argv[1];
  readRunList(run_list_path);

  // Resize vectors
  resizeVectors();

  // get luminosity
//  getLuminosity();

  // get hist
  getHist();

  // open root file
  system("mkdir -p result");
  rootFile = new TFile("result/dq-plot.root", "recreate");
  if (!rootFile->IsOpen()) {
    std::cout << "File open failed! " << std::endl;
    return 1;
  }
  
  // make gdirectory
  for (int l=0; l<NUMBEROFLAYER; l++) {
    for (int d=0; d<2; d++) {
      temp_strstream.str("");  temp_strstream << layerName2[l] << "/" << histName << "/" << subsubName[d];
      rootFile->mkdir(temp_strstream.str().c_str());
    }
  }
  for (int m=0; m<3; m++) rootFile->mkdir(methodName[m].c_str());

  saveOriHist();

  normalize(); 
  
  // calculate peak error
  for (int t=0; t<NUMBEROFTRY; t++){
//    getMaxBinNBin(2*t);
//    fitPeak(1, t);
    //calculatePeak(t);
  }
//  calculateError();
  
  //main
  for (int bin=1; bin<2; bin++){
    // decide nbin
//    getMaxBinNBin(2*bin);

    // mean method mear the peak
//    calculatePeak(bin);
//    plotHist();
//    plotGraph(1, 1);

    // mean method from 0 to 60 ToT
//    tookMean(bin);
//    plotHist();
//    plotGraph(1, 0);

    // gaus fitting one
//    fitPeak(1, bin);
    plotHist();
    //plotGraphAllLayer(run_list_name, "gaussian_fitting");
//    plotGraphLayer(run_list_name, "gaussian_fitting", 0);
//    normalizeTot(run_list_name, "gaussian_fitting", 0);
//    plotTotVsMuLayer(run_list_name, "gaussian_fitting", 0);
  }

  // close root file
  rootFile->Close();

  std::cout << "finished!" << std::endl;
  //gApplication->Terminate();

  return 0;
}
