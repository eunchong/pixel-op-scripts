#!/bin/bash

# arrray
runNum=(
324320
326439
327265
329484
332720
336497
337052
337662
338712
339070
339849
339957
)

DATE=`date +%y%m%d-%H%M`

cmake .. || (echo "cmake failed." && exit 1)

source x86_64-slc6-gcc62-opt/setup.sh

gmake -j4 || (echo "gmake failed." && exit 1)


for r in {0..11}; do
  inputDataSetName=data17_13TeV:data17_13TeV.00${runNum[r]}.express_express.merge.RAW
  outputDataSetName=user.eunchong.00${runNum[r]}.$DATE/

  if [ $r -eq 1 ]; then
    hundred=1
  elif [ $r -eq 5 -o $r -eq 9 ]; then 
    hundred=3
  else
    hundred=2
  fi
  if [ $r -eq 2 -o $r -eq 4 -o $r -eq 10 ]; then
    ten=1
  else
    ten=0
  fi

  for l in {0..9}; do
    if [ $l -eq 0 ]; then
      echo -e "data17_13TeV.00${runNum[r]}.express_express.merge.RAW._lb0${hundred}${ten}${l}._SFO-ALL._0001.1" > ../lumiList.txt
    else
      echo -e "data17_13TeV.00${runNum[r]}.express_express.merge.RAW._lb0${hundred}${ten}${l}._SFO-ALL._0001.1" >> ../lumiList.txt
    fi
  done

  cat ../lumiList.txt

  AMI_TAG=f933

  pathena \
    --trf "Reco_tf.py \
      --conditionsTag 'all:CONDBR2-BLKPA-2018-09' \
      --ignoreErrors 'False' \
      --autoConfiguration='everything' \
      --geometryVersion 'all:ATLAS-R2-2016-01-00-01' \
      --beamType 'collisions' \
      --AMITag '$AMI_TAG' \
      --postExec 'r2a:MSMgr.AddItemToAllStreams(\"xAOD::MuonAuxContainer#MuonsAux.-DFCommonMuonsTight.-DFCommonGoodMuon.-DFCommonMuonsMedium.-DFCommonMuonsLoose\")' \
      --preExec 'r2a:from InDetRecExample.InDetJobProperties import InDetFlags;' \
                'InDetFlags.useDynamicAlignFolders.set_Value_and_Lock(True);' \
                'TriggerFlags.AODEDMSet=\"AODFULL\";' \
                'rec.doAFP.set_Value_and_Lock(True);' \
                'DQMonFlags.doAFPMon=True;' \
                'all:DQMonFlags.doCTPMon=False;' \
                'DQMonFlags.doLVL1CaloMon=False;' \
                'DQMonFlags.doHLTMon=False;' \
                'DQMonFlags.doTRTMon=False;' \
                'DQMonFlags.doSCTMon=False;' \
                'DQMonFlags.doMissingEtMon=False;' \
                'DQMonFlags.doMuonTrackMon=False;' \
                'DQMonFlags.doMuonSegmentMon=False;' \
                'DQMonFlags.doMuonTrkPhysMon=False;' \
                'DQMonFlags.doMuonCombinedMon=False;' \
                'DQMonFlags.doLucidMon=False;' \
                'DQMonFlags.doJetTagMon=False;' \
                'DQMonFlags.doEgammaMon=False;' \
                'DQMonFlags.doMuonRawMon=False;' \
                'DQMonFlags.doTRTElectronMon=False;' \
                'DQMonFlags.doLArMon=False;' \
                'DQMonFlags.doTileMon=False;' \
                'DQMonFlags.doCaloMon=False;' \
                'DQMonFlags.doPixelMon=True;' \
                'DQMonFlags.doGlobalMon=False;' \
                'DQMonFlags.doInDetAlignMon=False;' \
                'DQMonFlags.doInDetGlobalMon=False;' \
                'DQMonFlags.doInDetPerfMon=False;' \
                'rec.doFwdRegion=False;' \
                'rec.doTau=False;' \
                'rec.doMuon=False;' \
                'rec.doEgamma=False;' \
                'rec.doMuonCombined=False;' \
                'rec.doCalo=False;' \
                'rec.doJetMissingETTag=False;' \
                'rec.doTrigger=False' \
      --steering='doRAWtoALL' \
      --outputFileValidation 'False' \
      --maxEvents=-1 \
      --inputBSFile %IN \
      --outputHISTFile '%OUT.HIST.root'" \
    --inDS $inputDataSetName \
    --inputFileList ../lumiList.txt \
    --outDS $outputDataSetName \
    --nFilesPerJob=1
done

#     AMIConfig : f781, f846, f889
